import mysql.connector
import datetime
import time
import holidays
import configparser
import os

os.chdir(str(os.path.dirname(os.path.abspath(__file__))))
config = configparser.ConfigParser()
config.read('config.ini')
output_path=config.get('main', 'PATH_GRAPH_OUTPUT')

LLC_lon=config.get('corridor', 'LLC_lon')
LLC_lat=config.get('corridor', 'LLC_lat')

URC_lon=config.get('corridor', 'URC_lon')
URC_lat=config.get('corridor', 'URC_lat')

HEIGHT=URC_lat=config.get('corridor', 'HEIGHT')

sqawks=eval(config.get('SQUAWK', 'interesting'))

cnx = mysql.connector.connect(user=config.get('main', 'DB_USERNAME'), password=config.get('main', 'DB_PASSWORD'),
                              host=config.get('main', 'DB_HOSTNAME_NAME'),
                              database=config.get('main', 'DB_NAME'))

cursor = cnx.cursor()

def get_number_today():
    date = datetime.datetime.now()
    date = date.replace(hour=0, minute=0, second=0)
    date = time.mktime(date.timetuple())
    date = str(date)
    com = "SELECT count(distinct(flightnr)) from DUMP1090DATA where seentime >" + date + ";"
    query = (com)
    cursor.execute(query)
    return cursor.fetchone()[0]


def get_avg_sun_IBK():
    query = ("select  count(distinct(flightnr))  from DUMP1090DATA"
            +" where DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday' and (lat  between "+LLC_lat+" and "+URC_lat+")"
            + " and (lon between "+LLC_lon+" and "+URC_lon+") and (height < " + HEIGHT + ");")
    cursor.execute(query)
    anzSunFlights = cursor.fetchone()
    query = ("select count(Distinct(DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W - %D - %M - %Y')))  from DUMP1090DATA"
            + " where DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday'"
            +" and (lat  between "+LLC_lat+" and "+URC_lat+") and"
            +" (lon between "+LLC_lon+" and "+URC_lon+")"
            + " and (height < " + HEIGHT + ");")
    cursor.execute(query)
    anzSun = cursor.fetchone()
    return (anzSunFlights[0] / anzSun[0])


def get_ts_30days():
    date = datetime.datetime.now() + datetime.timedelta(-30)
    return time.mktime(date.timetuple())


def get_ts_daystart(timestamp):
    date = datetime.datetime.fromtimestamp(timestamp)
    start = date.replace(hour=0, minute=0, second=0)
    return time.mktime(start.timetuple())


def get_ts_dayend(timestamp):
    date = datetime.datetime.fromtimestamp(timestamp)
    end = date.replace(hour=23, minute=59, second=59)
    return time.mktime(end.timetuple())


def get_1d_later(timestamp):
    date = datetime.datetime.fromtimestamp(timestamp)
    date = date + datetime.timedelta(1)
    return time.mktime(date.timetuple())


def get_ts_22(timestamp):
    date = datetime.datetime.fromtimestamp(timestamp)
    date = date.replace(hour=22, minute=0, second=0)
    return time.mktime(date.timetuple())


def get_ts_06(tiemstamp):
    date = datetime.datetime.fromtimestamp(tiemstamp)
    date = date.replace(hour=6, minute=0, second=0)
    return time.mktime(date.timetuple())


def get_ts_first_data():
    query = ("select min(seentime) from DUMP1090DATA;")
    cursor.execute(query)
    return cursor.fetchone()[0]


def write_holydays_table():
    endy =  datetime.date.today().year
    starty = datetime.datetime.fromtimestamp(get_ts_first_data()).year
    holys = holidays.AT(years=range(starty, endy+1), prov='T')
    query = ("create table if not exists holidays( "
            + "day date primary key, "
            + "name varchar(100)); ")
    cursor.execute(query)
    cnx.commit()
    query = ""
    for date, name in sorted(holys.items()):
        query = ("insert into holidays values ( '" + str(date) + "', '" + str(name) + "' ) "
            + " ON DUPLICATE KEY UPDATE day = day;")
        cursor.execute(query)
    cnx.commit()


def get_30days():
    date = get_ts_30days()
    start = get_ts_daystart(date)
    end = get_ts_dayend(start)
    l = []
    for i in range(0,30):
        starts = str(start)
        ends = str(end)
        query = ("select count(distinct(flightnr)) from DUMP1090DATA where seentime between " + starts + " and " + ends + ";")
        cursor.execute(query)
        l.append(cursor.fetchone()[0])
        start = get_1d_later(start)
        end = get_1d_later(end)
    return l


def get_nightflights_30days():
    date = get_ts_30days()
    start = get_ts_22(date)
    end = get_ts_06(start)
    l = []
    for i in range(0,30):
        starts = str(start)
        ends = str(end)
        query = ("select count(distinct(flightnr)) from DUMP1090DATA"
                +" where (lat  between "+LLC_lat+" and "+URC_lat+") and"
                +" (lon between "+LLC_lon+" and "+URC_lon+") and"
                +" (height < " + HEIGHT + ") and"
                +" (seentime between " +starts + " and " + ends +");")
        cursor.execute(query)
        l.append(cursor.fetchone()[0])
        start = get_1d_later(start)
        end = get_1d_later(end)
    return l


def get_max_date():
    query =("select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "group by datum "
            + "having count = (select MAX(A.COUNT) "
            + "from ( "
            + "select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "group by datum ) AS A);")
    cursor.execute(query)
    return  cursor.fetchone()[0]


def get_max_date_night():
    query =("select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "where TIME(FROM_UNIXTIME(seentime)) not between '06:00:00' and '22:00:00' "
            + "group by datum "
            + "having count = (select MAX(A.COUNT) "
            + "from ( "
            + "select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "where TIME(FROM_UNIXTIME(seentime)) not between '06:00:00' and '22:00:00' "
            + "group by datum ) AS A);")
    cursor.execute(query)
    return  cursor.fetchone()[0]


def get_max_date_sunday():
    query =("select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "where DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday' "
            + "group by datum "
            + "having count = (select MAX(A.COUNT) "
            + "from ( "
            + "select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "where DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday' "
            + "group by datum ) AS A);")
    cursor.execute(query)
    return  cursor.fetchone()[0]


def get_max_speed():
    query = ("SELECT MAX(speed) FROM DUMP1090DATA")
    cursor.execute(query)
    return cursor.fetchone()
 


def get_max_height():
    query = ("SELECT MAX(height) FROM DUMP1090DATA")
    cursor.execute(query)
    return cursor.fetchone()
  


def get_avg_holiday():
    query = ("SELECT COUNT(DISTINCT(flightnr)) FROM DUMP1090DATA "
            + "WHERE (lat  between "+LLC_lat+" and "+URC_lat+") and(lon between "+LLC_lon+" and "+URC_lon+") "
            +  " and (height < " + HEIGHT + ") AND Date(From_UNIXTIME(seentime)) in (select day from holidays);")
    cursor.execute(query)
    flights = cursor.fetchone()[0]
    query = ("SELECT count(DISTINCT(holidays.day)) FROM holidays "
            + "WHERE holidays.day IN ( SELECT Date(From_UNIXTIME(seentime)) FROM DUMP1090DATA);")
    cursor.execute(query)
    sundays = cursor.fetchone()[0]
    return flights/sundays


def get_max_date_holiday():
    query = ("select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA WHERE Date(From_UNIXTIME(seentime)) in (select day from holidays) "
            + "group by datum having count = (select MAX(A.COUNT) from ( "
            + "select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA WHERE Date(From_UNIXTIME(seentime)) in (select day from holidays) "
            + "group by datum) AS A) ")
    cursor.execute(query)
    return cursor.fetchone()[0]


def get_avg_night_nonworkday():
    query = ("SELECT count(DISTINCT(flightnr)) FROM DUMP1090DATA "
            + "WHERE (lat  between "+LLC_lat+" and "+URC_lat+") AND (lon between "+LLC_lon+" and "+URC_lon+") "
            + "AND (height < " + HEIGHT + ") AND (TIME(FROM_UNIXTIME(seentime)) not between "
            + "'06:00:00' and '22:00:00') AND ((Date(From_UNIXTIME(seentime)) in "
            + "(select day from holidays)) OR (DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday'))")
    cursor.execute(query)
    flights = cursor.fetchone()[0]
    query = ("SELECT COUNT(DISTINCT(DATE(FROM_UNIXTIME(seentime)))) FROM DUMP1090DATA "
            + "WHERE (TIME(FROM_UNIXTIME(seentime)) not between '06:00:00' and '22:00:00') AND "
            + "((Date(From_UNIXTIME(seentime)) in (select day from holidays)) OR "
            + "(DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday'));")
    cursor.execute(query)
    days = cursor.fetchone()[0]
    return float(flights)/float(days)

def get_last_interesting_flights():

    query ="SELECT From_UNIXTIME(seentime), squawk ,flightnr FROM DUMP1090DATA WHERE "+"squawk>="+str(sqawks.values()[0][0])+" AND squawk<="+str(sqawks.values()[0][1])
    
    for min,max in sqawks.values()[1:]:
        query+=" OR squawk>="+str(min)+" AND squawk<="+str(max)

    query+=" ORDER BY seentime DESC LIMIT 5"
    cursor.execute(query)
    flights = cursor.fetchall()

    return flights

def get_interesting_text(sqawk):
    
    for text, minmax in sqawks.iteritems():

        if sqawk>=minmax[0] and sqawk<=minmax[1]:
           return text
    
    return None

def get_output_path():
    return output_path

def get_apikey():
    return config.get('api', 'apikey')

def get_squawk_folder():
    return config.get('pictures','SQUAWK_FOLDER')

def get_SQUAWK_pictures():
    return eval(config.get('pictures','pics'))

def get_ranklist_flights():

    query="SELECT count(DISTINCT flightnr) as c,SUBSTRING(flightnr,1,3) FROM DUMP1090DATA group by SUBSTRING(flightnr,1,3) order by c DESC LIMIT 5"
    cursor.execute(query)
    flights = cursor.fetchall()

    return flights


write_holydays_table()