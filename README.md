#Flugzeugüberwachung

##Installationsverlauf:
```
git clone https://bitbucket.org/florian_neururer/aircraft-monitoring
cd aircraft-monitoring
pyhton setup.py
python visualization.py
```

Mindesvoraussetzungen sind **Python 2** mit funktionierendem **pip** um das Setup verwenden zu können!


Um alle notwendigen Abhänigkeiten zu installieren muss zuerst das Setup-script ausgeführt werde:

```
python setup.py
```

Damit die Visualisierung funktiionier muss die MySQL-Datenbank gestartet sein.
Dann kann die **config.ini** Konfiguraitionsdatei modifiziert werden.

Das Bild wird mithilfe folgendes Aufrufs generiert

```
python visualization.py
```
