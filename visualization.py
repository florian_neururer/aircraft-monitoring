import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime as dt
import aircraft
import matplotlib.image as mpimg
from matplotlib.gridspec import GridSpec
import random
from PIL import Image

fig = plt.figure(figsize=(12.8, 10.24), dpi=300, facecolor='black')
gs = GridSpec(3, 4)

ax = fig.add_subplot(111,facecolor='black')

ax.text(0, 1.1, "Informationswerte", fontsize=17, color='white',weight="bold")

ax.text(-0.07, 1.06, "Meistbeflogener Tag: {0}".format(aircraft.get_max_date()), fontsize=12, color='white',alpha=0.8)
ax.text(-0.07, 1.02, "Meistbeflogene Nacht: {0}".format(aircraft.get_max_date_night()), fontsize=12, color='white',alpha=0.8)
ax.text(-0.07, 0.98, "Meistbeflogener Sonntag: {0}".format(aircraft.get_max_date_sunday()), fontsize=12, color='white',alpha=0.8)
ax.text(-0.07, 0.94, "Meistbeflogener Feiertag: {0}".format(aircraft.get_max_date_holiday()), fontsize=12, color='white',alpha=0.8)
ax.text(-0.07, 0.90, "Hoechstgemessene Geschwindigkeit: {0} km/h".format(aircraft.get_max_speed()[0]), fontsize=12, color='white',alpha=0.8)
ax.text(-0.07, 0.86, "Hoechstgemessene Flughoehe: {0} m".format(aircraft.get_max_height()[0]), fontsize=12, color='white',alpha=0.8)
ax.text(-0.07, 0.82, "Durchschnittle Fluege am Sonntag: {0}".format(aircraft.get_avg_sun_IBK()), fontsize=12, color='white',alpha=0.8)
ax.text(-0.07, 0.78, "Flugzeuge heute: {0}".format(aircraft.get_number_today()), fontsize=12, color='white',alpha=0.8)
ax.text(-0.07, 0.74, "Durchschnittle Fluege am Feiertag: {0}".format(aircraft.get_avg_holiday()), fontsize=12, color='white',alpha=0.8)
ax.text(-0.07, 0.70, "Nachtfluege an nicht Werktagen: {0}".format(aircraft.get_avg_night_nonworkday()), fontsize=12, color='white',alpha=0.8)


ax.text(0.70, 1.1, "Rankliste", fontsize=17, color='white',weight="bold")

ax.text(0.40, 1.03, "Rang", fontsize=12, color='white',weight="bold")
ax.text(0.50, 1.03, "Kuerzel", fontsize=12, color='white',weight="bold")
ax.text(0.63, 1.03, "Fluganzahl", fontsize=12, color='white',weight="bold")
ax.text(0.82, 1.03, "Fluggesellschaft", fontsize=12, color='white',weight="bold")

import urllib2
import json

operators=""
ranklist=aircraft.get_ranklist_flights()
for operator in ranklist:
    operators+=operator[1]+","

operators=operators[:-1]


contents = urllib2.urlopen("https://v4p4sz5ijk.execute-api.us-east-1.amazonaws.com/anbdata/airlines/designators/code-list?api_key=" + aircraft.get_apikey() + "&format=json&states=&operators="+operators).read()
companies = json.loads(contents)


for i in range(0,5):
    ax.text(0.42, 0.98-0.05*i, i+1, fontsize=12, color='white')
    ax.text(0.52, 0.98-0.05*i, ranklist[i][1], fontsize=12, color='white')
    ax.text(0.66, 0.98-0.05*i, ranklist[i][0], fontsize=12, color='white')
    for com in companies:
        if com["operatorCode"]==ranklist[i][1]:
            ax.text(0.78, 0.98-0.05*i, com["operatorName"], fontsize=12, color='white')
    


plt.subplots_adjust(wspace=0.5,)

daily30=fig.add_subplot(gs[1, :-2])
daily30.patch.set_alpha(0)
daily30.plot(range(30),aircraft.get_30days())
daily30.set_title("Flugzeuge pro Tag",color="red")
daily30.yaxis.label.set_color('red')
daily30.tick_params(axis='x', colors='red')
daily30.tick_params(axis='y', colors='red')
daily30.spines['left'].set_color('red')
daily30.spines['bottom'].set_color('red')
daily30.set_ylabel("Anzahl der Fluege")
daily30.set_xticks(range(1,31,2))

daily_night_30=fig.add_subplot(gs[1, 2:])
daily_night_30.patch.set_alpha(0)
daily_night_30.set_title("Flugzeuge zw 22:00 - 6:00",color="red")
daily_night_30.bar(range(30),aircraft.get_nightflights_30days())
daily_night_30.yaxis.label.set_color('red')
daily_night_30.tick_params(axis='x', colors='red')
daily_night_30.tick_params(axis='y', colors='red')
daily_night_30.spines['left'].set_color('red')
daily_night_30.spines['bottom'].set_color('red')
daily_night_30.set_ylabel("Anzahl der Fluege")
daily_night_30.set_xticks(range(1,31,2))


ax.text(-0.07, 0.25, "Datum", fontsize=17, color='white',weight="bold")
ax.text(0.1, 0.25, "Flugnummer", fontsize=17, color='white',weight="bold")
ax.text(0.33, 0.25, "SQWAK", fontsize=17, color='white',weight="bold")
ax.text(0.52, 0.25, "Bedeutung", fontsize=17, color='white',weight="bold")
ax.text(0.88, 0.25, "Bild", fontsize=17, color='white',weight="bold")


flights=aircraft.get_last_interesting_flights()
flights_with_picture=[]
pic_flight=[None]

import re

for f in flights:
    for reg,path in aircraft.get_SQUAWK_pictures().iteritems():
        pattern=re.compile(reg)
        if re.match(pattern,f[2]):
            flights_with_picture.append((f,path))

if len(flights_with_picture)!=0:

    pic_flight = flights_with_picture[random.randint(0,len(flights_with_picture)-1)]
    implot=fig.add_subplot(gs[2, 3:])
    img = Image.open(aircraft.get_squawk_folder()+pic_flight[1])
    img.thumbnail((230, 230), Image.ANTIALIAS)
    implot.imshow(img)

i=0

for flight in flights:
    
    color = 'white'
    if pic_flight[0]==flight:
        color='red'

    ax.text(-0.11, 0.20-0.05*i, flight[0], fontsize=12, color=color,weight="bold",alpha=0.8)
    ax.text(0.15, 0.20-0.05*i, flight[2], fontsize=12, color=color,weight="bold",alpha=0.8)
    ax.text(0.36, 0.20-0.05*i, flight[1], fontsize=12, color=color,weight="bold",alpha=0.8)
    ax.text(0.52, 0.20-0.05*i, aircraft.get_interesting_text(flight[1]), fontsize=12, color=color,weight="bold",alpha=0.8)
    i+=1


ax.text(0.85, -0.1, "created " + dt.datetime.now().strftime("%Y-%m-%d %H:%M"), fontsize=10, color='white', alpha=0.25)
fig.savefig(aircraft.get_output_path(), facecolor=fig.get_facecolor())