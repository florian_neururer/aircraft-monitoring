import pip

def install():
    pip.main(['install', "matplotlib"])
    pip.main(['install', "Pillow"])
    pip.main(['install', "holidays"])
    pip.main(['install', "mysql-connector"])
    pip.main(['install', "configparser"])


if __name__ == '__main__':
    install()